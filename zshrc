# setup zsh completions
autoload -Uz compinit
compinit

# enable command history
HISTFILE="$HOME/.zsh_history"
HISTSIZE=10000
SAVEHIST=10000

## History command configuration
setopt extended_history
setopt hist_expire_dups_first
setopt hist_find_no_dups      
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_verify
setopt inc_append_history
setopt share_history

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='vim'
fi

# set aliases
alias ls='ls --color=auto'

# powerlevel10k prompt
source ~/.config/powerlevel10k/powerlevel10k.zsh-theme
# source ~/.config/powerlevel10k/config/p10k-lean.zsh

# enable pacman plugins
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down


# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"


# ENV
POWERLEVEL9K_DISABLE_CONFIGURATION_WIZARD=true
